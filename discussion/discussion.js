db.course_bookings.insertMany([
			{"courseId": "C001", "studentId": "S004", "isCompleted": true},
			{"courseId": "C002", "studentId": "S001", "isCompleted": false},
			{"courseId": "C001", "studentId": "S003", "isCompleted": true},
			{"courseId": "C003", "studentId": "S002", "isCompleted": false},
			{"courseId": "C001", "studentId": "S002", "isCompleted": true},
			{"courseId": "C004", "studentId": "S003", "isCompleted": false},
			{"courseId": "C002", "studentId": "S004", "isCompleted": true},
			{"courseId": "C003", "studentId": "S007", "isCompleted": false},
			{"courseId": "C001", "studentId": "S005", "isCompleted": true},
			{"courseId": "C004", "studentId": "S008", "isCompleted": false},
			{"courseId": "C001", "studentId": "S013", "isCompleted": true}
	])

db.course_bookings.aggregate([
		{
			$group: { _id: null, count: { $sum: 1 } }
		}

	]);

db.course_bookings.aggregate([
		{
			$match: { "isCompleted": true }
		},
		{
			$group: { _id: "$courseId", total: { $sum: 1 } }
		}

	]);

db.course_bookings.aggregate([
		{
			$match: { "isCompleted": true }
		},
		{
			$project: { "studentId": 0}
		}
	]);

//1 to show 0 to hide

db.course_bookings.aggregate([
		{
			$match: { "isCompleted": true }
		},
		{
			$sort: { "courseId": -1}
		}
	]);

//-1 for descending order

db.course_bookings.aggregate([
		{
			$match: { "isCompleted": true }
		},
		{
			$sort: { "studentId": 1}
		},
		{
			$project: { "courseId": 1, "studentId": 1, "_id":0}
		}
	]);


//Mini Activity 

//no need to sort if only 1 document

db.course_bookings.aggregate([
		{ $match: { "studentId": "S013", "isCompleted": true }},
		{ $group: {_id: null, totalNumberOfCompletedCourse: {$sum: 1 } } }
]);

db.course_bookings.aggregate([
		{ $match: { "studentId": "S013", "isCompleted": true }},
		{ $group: {_id: "StudentId", totalNumberOfCompletedCourse: {$sum: 1 } } }
]);

/*
	Syntax:
		{ $count: <nameOfTheOutputFieldWhichHasItsCountAsItsValue> }
*/

db.course_bookings.aggregate([
		{ $match: { "studentId": "S013", "isCompleted": true }},
		{ $count: "totalNumberOfCompletedCourse" }
]);


db.course_bookings.aggregate([
		{ $sort: {"courseId": -1, "studentId": 1 }}
	]);

db.orders.insertMany([
		{
			"cust_Id": "A123",
			"amount": 500,
			"status": "A"
		},
		{
			"cust_Id": "A123",
			"amount": 250,
			"status": "A"
		},
		{
			"cust_Id": "B212",
			"amount": 200,
			"status": "A"
		},
		{
			"cust_Id": "B212",
			"amount": 200,
			"status": "D"
		}
]);

db.orders.aggregate([
		{ $match: {"status": "A"}},
		{ $group: {_id: "$cust_Id", maxAmount: { $max: "$amount" }}}
	]);

/*
	
	$sum operator - will total the values
	$max operator - will show you the highest value
	$min operator - will show your the lowest value
	$avg operator - will get the average value

